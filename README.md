# Formation GitLab

## Théorie
### Introduction 

GitLab est un logiciel libre basé sur git. Git est un outil de versionning, permettant de garder une trace et de gérer plusieurs versions d'un projet.
Pour comprendre comment fonctionne Git, nous allons utiliser une analogie : un projet git peut être imaginé comme une étagère contenant différents livres.
Tout d'abord nous allons récupèrer l'étagère déjà présente en faisant un `git clone`.
A chaque fois que l'on souhaite déposer une version sur l'étagère pour quelle soit accessible sur le web on va sélectionner les modifications que l'on souhaite partager `git add`, on dépose une étiquette pour savoir à quoi correspond cette modification `git commit -m`. Une fois la sélection et l'étiquette mise on peut déposer la version sur le web `git push`. Un personne souhaite mettre à jour sa version avec celle sur le web elle devra appuyer sur un bouton pour les récupérer `git pull`. 

Les commandes principales sont : 

- `git clone <nomProjet>` : Recupère le projet et créer un dossier dans lequel il est stocké
- `git add <nomFichier>` : Ajoute les fichiers qui sont à envoyer sur le git 
- `git status` : Affiche l'état du projet
- `git commit -m "je suis un commit"` : Ajoute un nom aux modifications
- `git push` : Pousse les modifications sur le git 
- `git pull` : Réalise une mise à jour entre la machine et le git 

Les commandes de gestion de branches : 

- `git branch <nomBranche>` : Créer une branche 
- `git checkout <nomBranche>` : Se place sur la branche
- `git branch -d <nomBranche>` : Supprime la branche

## Installation 

Si vous n'avez pas git sur votre ordinateur il suffit de le télécharger sur le site de [Git](https://git-scm.com/downloads)

## Stockage des indentifiants sur Rstudio

Sur Rstudio il est possible de stocker ses identifiants une seule fois pour avoir a éviter de les répéter a chaque push/pull

Pour cela il faut lancer les lignes suivantes dans la console R directement 

```R
library(credentials)
git_credential_update('https://gitlab.depp.education.fr')
```

*Un terminal de saise va s'ouvrir vous devez mettre votre login et mot de passe*

## Bonnes pratiques

- **Aucun fichier de données ne doit être mis sur gitlab, l'outil est fait pour stocker des programmes/documentation**
- **Aucun secret ne doit être mis sur gitlab, par exemple des code de base de données**
- **Chaque projet doit avoir un .gitignore pour éviter de mettre en ligne des fichiers de données**
- Il faut essayer de commit à chaque fois qu'on a une nouvelle fonctionnalité
- Il faut éviter de commit trop de modifications d'un coup, cela rend compliqués les retours arrière
- Les messages de commit doivent être le plus clair possible
- Pensez a toujours commit vos avancées avant de quitter votre session
- Il est possible d'ajouter des emoji au moment du commit [liste](https://gitmoji.dev/) chaque emoji a une signification ce qui permet une meilleure lisibilité



## Pour aller plus loin

Tutoriel avancé avec Rstudio : https://collaboratif-git-formation-insee.netlify.app/  
Défénitions des droits par rôles : https://gitlab.depp.education.fr/help/user/permissions

## Pratique 

### Activités 1 : Git sur Rstudio

Info : La partie Git se trouve à droite
Lancer Rstudio 
1. Ouvrir un projet avec git

<details>
  <summary>Solution</summary>
  
  Cliquer sue File > New Project

  ![image.png](./img/rstudio_opengit.png)

  Version Control -> Git

  Dans Repository URL il faut mettre le lien du dépot, vous le trouverez sur la page du projet en cliquant sur le bouton "Clone", il faut prendre le deuxième liens pour ce connecter avec le login/mot de passe

  ![image-1.png](./img/image-1.png)

  Les autres champs ce remplisse automatiquement.

  ![image.png](./img/image.png)

  Votre login et mot de passe vous seront demandés :

  ![image-2.png](./img/image-2.png)

   Une fois le projet cloner vous avez l'onglet Git de disponible : 

  ![image-3.png](./img/image-3.png)

</details>

2. Ajouter un fichier sur git  

<details>
  <summary>Solution</summary>
  
  Une fois le fichier créé sur votre environnement Rstudio, vous devez aller sur l'onglet Git.

  ![image-4.png](./img/image-4.png)

  *Vous allez voir votre nouveau fichier avec comme statut ? ?, Cela veut dire qu'il n'est pas encore sur git*

  Pour ajouter un fichier sur git, cela se décompose en deux étapes, commit le fichier (permet de le sauvegarder localement) et push (envoyer l'ensemble de commits sur le serveur git)

  Sur l'interface, il vous faut cliquer sur la case a coté du fichier a ajouter, la passera en A
  ![image-5.png](./img/image-5.png)

  Une fois le fichier ajouté, vous devez cliquer sur Commit. Une fenêtre sur votre navigateur va s'ouvrir.  
  Elle vous montre les changements effectués sur le fichier à ajouter, et vous permet de mettre un message de commit.  
  **Vous devez mettre un message de commit, celui-ci doit résumer les actions effectuer sur le projet**

  ![image-6.png](./img/image-6.png)

  Une fois le commit effectué, vous allez avoir le message suivant sur l'onglet git :

  ![image-7.png](./img/image-7.png)

  Cela signifie que le commit est fait localement, il faut maintenant le pousser (push) sur le serveur.

  Pour cela vous devez cliquer sur le bouton "Push" et rentrez vos identifiants

  Vous pouvez maintenant voir votre fichier sur gitlab : 

  ![image-8.png](./img/image-8.png)

</details>

3. Modifier un fichier sur votre projet  

<details>
  <summary>Solution</summary>
  
  Une fois votre fichier modifié sur Rstudio, votre onglet Git est mis à jour :

  ![image-9.png](./img/image-9.png)

  Votre fichier est passé au statut modifié, pour mettre en ligne la modification, il faut cocher la case et faire un commit.

  Une fenêtre similaire a l'étape 2 vous s'ouvrir, vous pourrez voir les modifications apporté a votre fichier, il faut ajouter un message de commit.

  ![image-10.png](./img/image-10.png)

  Ensuite vous pouvez push votre commit, sur gitlab vous pourrez voir la modification dans les commits :

  ![image-11.png](./img/image-11.png)

</details>

4. Déplacer un fichier dans un dossier  

<details>
  <summary>Solution</summary>
  
  Une fois votre dossier créé et le fichier déplacer à l'intérieur vous allez avoir deux lignes dans l'onglet git 

  ![image-12.png](./img/image-12.png)

  Les deux lignes précise que le fichier a été supprimer et qu'un nouveau dossier a été créer, quand on coche les deux cases une nouvelle apparaît a la place des deux autres :

  ![image-13.png](./img/image-13.png)

  Git a compris qu'il s'agit d'un déplacement de fichier (renamed), pour continuer, il faut commit et push.

</details>

5. Récupérer les changements (pull)  

<details>
  <summary>Solution</summary>
  
  Git permet de travailler en équipe, de ce fait, vous allez souvent devoir récupérer (pull) le code d'autres membres du projet.

  **Il est recommandé de toujours pull avant de push, cela vous permet et d'avoir les bonnes versions de code**

  Pour récupérer le code, il faut cliquer sur pull 

  ![image-14.png](./img/image-14.png)

![image-15.png](./img/image-15.png)

  Par exemple dans ce cas, on vois que le fichier fichier_test.R a été mis à jour.

</details>


### Activités 2 : Git en ligne de commande 

1. Cloner le projet 

Que signifie cloner un repositories ? <br/>
Ne pas oublier si on à cérer une clé ssh de cloner avec un lien spécifique. <br/>
Quelle commande doit-on utiliser pour le cloner ? 

Information : Pour la depp il faut changer par gitlab.depp.in.adc 

<details>
  <summary>Solution</summary>
  
  `git clone git@gitlab.depp.in.adc.education.fr:cisad/formation-gitlab.git`<br/>
  `cd formation-gitlab`

</details>

2. Créer une nouvelle branche 

Quel commande doit-on utiliser pour créer une nouvelle branche ? <br/>
Quel est l'utilité de créer une nouvelle branche ?

<details>
  <summary>Solution</summary>
  
  `git branch formation` <br/>
  `git checkout formation` 

</details>

3. Envoyer un nouveau fichier sur le git

Ne pas oublier de se placer sur la branche fraîchement créée.
Quels sont les commandes à utiliser ? 

<details>
  <summary>Solution</summary>
  
  `touch formation.txt` <br/>
  `git add formation.txt`<br/>
  `git status`<br/>
  `git commit -m ":sparkles: add new file"`
  `git push`

  Aller sur gitlab et vérifier que la branche est bien présente ainsi que son fichier 

</details>


### Générer clé ssh (Optionnel)

[Génération clé SSH](/GenerationSsh.md)
