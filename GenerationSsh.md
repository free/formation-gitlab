# Générer une clé ssh

##  Pourquoi installer une clé ssh ?
<br/>

Générer une clé ssh est utile pour la sécurité de votre code, par exemple elle permet d'éviter qu'une personne écrit des commits à votre place. Chaque clé est unique pour chaque ordinateur.

## Générer sa clé

1. Accéder au terminal

Une fois sur le terminal taper la commande `ssh-keygen`
Se souvenir de l'endroit ou a été stocké la clé
![SSH Keys](/img/cmd.PNG "ssh keys")

2. Connexion sur GitLab

Une fois connecter diriger vous vers les paramètres du compte (carré vert), cliquer sur l'onglet ssh keys (carré violet)
![SSH Keys](/img/git.PNG "ssh keys")


3. Retrouver la clé ssh dans l'explorateur de fichier

Ouvrir le fichier ayant pour extension .pub <br/>
Copier les données qui se trouve dans le fichier
![SSH Keys](/img/cmd2.PNG "ssh keys")

4. Retourner dans la partie ssh keys sur GitLab

Coller dans le carré prévu à cet effet. (Key)
Vous pouvez ajouter un titre à votre clé mais cela reste optionnel. (Utile si vous en avez plusieurs).
![SSH Keys](/img/git2.PNG "ssh keys")

5. Ajouter la clé

Vous n'avez plus qu'à cliquer sur : "Add key"
Voilà votre clé ssh à été créer sur votre gitLab, vous n'avez plus besoin de mot de passe pour faire des modifications sur votre repo.